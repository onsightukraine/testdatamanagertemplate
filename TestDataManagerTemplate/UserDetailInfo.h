//
//  UserDetailInfo.h
//  TestDataManagerTemplate
//
//  Created by Nikolay Chaban on 4/6/17.
//  Copyright © 2017 Nikolay Chaban. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDetailInfo : NSObject

@property (strong, nonatomic) NSString* userName;

@property (assign, nonatomic) NSUInteger userAge;

@end
