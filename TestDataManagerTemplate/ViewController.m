//
//  ViewController.m
//  TestDataManagerTemplate
//
//  Created by Nikolay Chaban on 4/6/17.
//  Copyright © 2017 Nikolay Chaban. All rights reserved.
//

#import "ViewController.h"

// Classes
#import "DataManager+User.h"
#import "UserDetailInfo.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UserDetailInfo* userInfo = [UserDetailInfo new];
    
    userInfo.userName = @"Vasya";
    userInfo.userAge  = 26;
    
    [[DataManager sharedInstance] addNewUserWithInfo: userInfo];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
