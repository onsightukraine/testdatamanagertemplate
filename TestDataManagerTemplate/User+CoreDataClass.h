//
//  User+CoreDataClass.h
//  TestDataManagerTemplate
//
//  Created by Nikolay Chaban on 4/6/17.
//  Copyright © 2017 Nikolay Chaban. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface User : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "User+CoreDataProperties.h"
