//
//  DataManager+User.m
//  TestDataManagerTemplate
//
//  Created by Nikolay Chaban on 4/6/17.
//  Copyright © 2017 Nikolay Chaban. All rights reserved.
//

#import "DataManager+User.h"

#import "User+CoreDataClass.h"

@implementation DataManager (User)

- (void) addNewUserWithInfo: (UserDetailInfo*) info
{
    User* userInfo = [self fetchUserWithName: info.userName];
    
    if ( userInfo == nil )
    {
        userInfo = [self insertNewObjectForEntityForName: @"User"];
    }
    
    userInfo.name = info.userName;
    userInfo.age  = info.userAge;
    
    [self saveContext];
}

- (User*) fetchUserWithName: (NSString*) name
{
    NSPredicate* namePredicate = [NSPredicate predicateWithFormat: @"name == %@", name];
    
    User* user = [self fetchObjectForEntityForName: @"User"
                                     withPredicate: namePredicate];
    
    return user;
}

@end
