//
//  DataManager+User.h
//  TestDataManagerTemplate
//
//  Created by Nikolay Chaban on 4/6/17.
//  Copyright © 2017 Nikolay Chaban. All rights reserved.
//

#import "DataManager.h"

// Classes
#import "UserDetailInfo.h"

@interface DataManager (User)

- (void) addNewUserWithInfo: (UserDetailInfo*) userInfo;

@end
